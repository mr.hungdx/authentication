<?php

return [
    'user' => [
        'model' => '\App\Models\User',
    ],

    'token' => [
        'cookie_name'  => md5('hungdx_remember_token'),
        'session_name' => 'hungdx_remember_token',

        // Thời gian sống của token. Nếu quá thời gian này thì token sẽ được coi là không hợp lệ
        'lifetime'  => 43200,   // in seconds, 12 hours

        // Thời gian tối đa trước khi update lại thời gian hết hạn của token
        'extend_lifetime_interval' => 900,          // in seconds, 15 minutes

        // Thuật toán dùng để mã hóa JWT. xem thêm $supported_algs at vendor/firebase/php-jwt/src/JWT.php
        'jwt_algorithm' => 'HS256',
    ],
    'session' => [
        // Thời gian xác định xem user có đang online hay không: đơn vị seconds
        'online' => 900, // in seconds, 15 minutes
    ]
];