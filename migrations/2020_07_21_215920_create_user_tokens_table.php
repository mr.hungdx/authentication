<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tokens', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('token', 255);
            $table->string('session_id', 255);
            $table->dateTime('expired_at');
            $table->timestamps();

            $table->primary(['user_id', 'token']);
            $table->index(['user_id', 'token']);
            $table->index(['expired_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tokens');
    }
}
