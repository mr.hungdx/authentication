## Authentication for laravel
- More secure: token stored in database
- Support authenticate with api mode

### Required
- PHP >= 7.2
- firebase/php-jwt

### Install
#### 1. Install packages
```
composer required firebase/php-jwt
composer required hungdx/authentication
``` 

#### 2. Register Provider
Add  `AuthenticationServiceProvider` to `config/app.php`
```php
<?php

return [
    // ...
    'providers' => [
        // ...
        HungDX\Authentication\AuthenticationServiceProvider::class,
    ]
    // ...
];
```

#### 3. Implement contracts
create file `app/Observers/AuthenticationObserver.php`

```php
<?php

namespace App\Observers;

class AuthenticationObserver implements \HungDX\Authentication\AuthenticationObserverContract
{
    use \HungDX\Authentication\AuthenticationObserverHelper;
}
```

Implement contract `HungDX\Authentication\UserRepositoryContract.php`
```php
<?php

class UserRepository implements \HungDX\Authentication\UserRepositoryContract
{
    // ...
    public function getUserByLoginName(string $login_name)
    {
        // implement code
    }

    public function getUserByUserId(int $user_id)
    {
        // implement code
    }
}

```

 #### 4. Binding class
Edit `app/Providers/AppServiceProvider.php`
```php
<?php

namespace App\Providers;

// ...
class AppServiceProvider extends ServiceProvider
{
    // ...
    public function register()
    {
        // ...
        $this->app->bind(\HungDX\Authentication\UserRepositoryContract::class, UserRepository::class);
        $this->app->bind(\HungDX\Authentication\AuthenticationObserverContract::class, AuthenticationObserver::class);
        // ...
    }
}
``` 