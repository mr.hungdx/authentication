<?php


namespace HungDX\Authentication;


use HungDX\Authentication\Guards\ApiGuard;
use HungDX\Authentication\Middlewares\ApiGuardMiddleware;
use HungDX\Authentication\Middlewares\LoginViaCookieMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;


class AuthenticationServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->initConfigs();
        $this->registerResources();
        $this->registerMiddlewares();
        $this->registerGuards();
    }

    private function initConfigs()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../configs/hungdx_auth.php', 'hungdx_auth',
        );

        $this->loadMigrationsFrom(__DIR__ . '/path/to/migrations');

        $this->app->singleton(AuthenticationService::class, function ($app) {
            return new AuthenticationService(
                app(UserRepositoryContract::class),
                app(AuthenticationObserverContract::class),
            );
        });
    }

    private function registerMiddlewares()
    {
        $this->app['router']->aliasMiddleware('login-via-cookie', LoginViaCookieMiddleware::class);
    }

    private function registerResources()
    {
        $this->publishes([
            __DIR__ . '/../configs/hungdx_auth.php' => config_path('hungdx_auth.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../migrations/' => database_path('/migrations')
        ], 'migrations');
    }

    private function registerGuards()
    {
        Auth::extend('api', function ($app, $name, array $config) {
            $guard = new ApiGuard(
                Auth::createUserProvider($config['provider'] ?? null),
                $app['request'],
                $config
            );
            $app->refresh('request', $guard, 'setRequest');
            return $guard;
        });
    }
}