<?php

namespace HungDX\Authentication;

use Carbon\Carbon;
use Firebase\JWT\JWT;
use HungDX\Authentication\Models\UserToken;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class AuthenticationService
{
    const PACKAGE_CONFIG_NAME = 'hungdx_auth';

    private UserRepositoryContract $user_repo;

    private AuthenticationObserverContract $obsever;

    public function __construct(
        UserRepositoryContract $user_repo,
        AuthenticationObserverContract $observer
    )
    {
        $this->user_repo = $user_repo;
        $this->obsever = $observer;
    }

    public static function getConfig($name)
    {
        return config(self::PACKAGE_CONFIG_NAME . '.' . $name);
    }

    public function getAuthGuardName(): string
    {
        if ($this->obsever) {
            return $this->obsever->getGuardName() ?: config('auth.defaults.guard');
        }
        return config('auth.defaults.guard');
    }

    /**
     * Login with username/email and password
     * @param string $login_name
     * @param string $password
     * @throws AuthenticationException
     */
    public function doLogin(string $login_name, string $password)
    {
        $user = $this->user_repo->getUserByLoginName($login_name);

        if (!$user || empty($user->password)) {
            throw new AuthenticationException();
        }

        // If password hash by md5 -> upgrade password hash
        if (strlen($user->password) === 32 && md5($password) === $user->password) {
            $user->password = Hash::make($password);
            $user->save();
        }

        if (!Hash::check($password, $user->password)) {
            throw new AuthenticationException();
        }

        // Recreate password hash code
        if (Hash::needsRehash($user->password)) {
            $user->password = Hash::make($password);
            $user->save();
        }

        // Create token
        $token = new UserToken();
        $token->user_id = $user->getKey();

        if ($this->obsever && $this->obsever->onBeforeLogin($token) === false) {
            throw new AuthenticationException();
        }

        // Set user to session
        $this->checkLimitNumberDevicesOnline($user, $token);
        Auth::guard($this->getAuthGuardName())->login($user);
        $token->refreshToken($this->getTokenLifeTime($user));
        $this->updateUserSession($token);

        if ($this->obsever) {
            $this->obsever->onAfterLogin($token, [
                'LOGIN_USING_ACCOUNT' => 1,
            ]);
        }
    }

    /**
     * Login via token
     */
    public function doLoginViaCookie()
    {
        if ($this->getCurrentUser()) {
            return;
        }

        $remember_token = Cookie::get(self::getConfig('token.cookie_name'));
        if (!$remember_token) {
            return;
        }

        $token = $this->doLoginViaToken($remember_token, true);
        if (!$token) {
            Cookie::forget(self::getConfig('token.cookie_name'));
        }

        if ($token && $this->obsever) {
            $this->obsever->onAfterLogin($token, [
                'LOGIN_USING_COOKIE' => 1,
            ]);
        }
    }

    public function doLoginViaToken(string $remember_token, bool $update_session = true): ?UserToken
    {
        try {
            $payload = JWT::decode($remember_token, config('app.key'), [self::getConfig('token.jwt_algorithm')]);
            $token = UserToken::getFromToken($payload->token ?? '', $payload->user_id ?? '');
        } catch (\Exception $exception) {
            return null;
        }

        if (!$token || !$token->user) {
            return null;
        }

        if ($token->isExpired()) {
            $token->delete();
            return null;
        }

        if ($this->obsever && $this->obsever->onBeforeLogin($token) === false) {
            throw new AuthenticationException();
        }

        $this->checkLimitNumberDevicesOnline($token->user, $token);

        Auth::guard($this->getAuthGuardName())->login($token->user);
        $token->refreshToken($this->getTokenLifeTime($token->user));

        if ($update_session) {
            $this->updateUserSession($token);
        }

        return $token;
    }

    public function getTokenLifeTime($user): int
    {
        if ($this->obsever) {
            $lifetime = $this->obsever->getTokenLifetime($user);
        }
        return isset($lifetime) && $lifetime > 0 ? $lifetime : self::getConfig('token.lifetime');
    }

    protected function updateUserSession(UserToken $token)
    {
        session([self::getConfig('token.session_name') => $token]);
        Cookie::queue(
            self::getConfig('token.cookie_name'),
            JWT::encode([
                'user_id' => $token->user_id,
                'token' => $token->token,
                'expired_at' => $token->expired_at->format("Y-m-d H:i:s"),
            ], config('app.key'), self::getConfig('token.jwt_algorithm')),
            $token->expired_at->diffInMinutes(Carbon::now())
        );
    }

    public function extendTokenLifeTime(?UserToken $token = null, bool $update_session = true)
    {
        if (is_null($token)) {
            $token = $this->getCurrentToken();
        }

        if (!$token) {
            return;
        }

        if (Carbon::now()->diffInSeconds($token->update) < self::getConfig('token.extend_lifetime_interval')) {
            return;
        }

        $token->refreshToken($this->getTokenLifeTime($token->user));
        $this->updateUserSession($token, true);
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User|\App\User|Model|null
     */
    public function getCurrentUser()
    {
        return Auth::guard(self::getAuthGuardName())->user() ?: null;
    }

    public function isLoggedIn(): bool
    {
        return !!$this->getCurrentUser();
    }

    public function getCurrentToken(bool $refresh = false): ?UserToken
    {
        $user = $this->getCurrentUser();
        $token = session(self::getConfig('token.session_name'));
        if (!$token || !$user) {
            return null;
        }

        if ($user->getKey() !== $token->user_id) {
            return null;
        }

        if ($refresh) {
            $token->fresh();
        }
        return $token;
    }

    public function doLogout()
    {
        $user = $this->getCurrentUser();
        if (!$user) {
            return;
        }

        $token = $this->getCurrentToken();
        Cookie::queue(Cookie::forget(self::getConfig('token.cookie_name')));
        Session::forget(self::getConfig('token.session_name'));
        Auth::guard($this->getAuthGuardName())->logout();

        if ($token) {
            $token->delete();
        }

        if ($this->obsever) {
            $this->obsever->onLogout($user, $token);
        }
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User|\App\User|Model|mixed $user
     * @param UserToken|null $token
     * @throws LimitDevicesPerUserException
     */
    public function checkLimitNumberDevicesOnline($user, ?UserToken $token = null)
    {
        if ($this->obsever) {
            $max_devices = $this->obsever->getNumberLimitedDevicesOnline($user);
        }
        if (!isset($max_devices) || $max_devices <= 0) {
            return;
        }

        $count_online = UserToken::getByUserId($user->getKey())
            ->where('expired_at', '>', Carbon::now())
            ->where('token', '!=', $token->token)
            ->count();

        if ($count_online >= $max_devices) {
            throw new LimitDevicesPerUserException();
        }
    }
}
