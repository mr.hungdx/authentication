<?php


namespace HungDX\Authentication;


use HungDX\Authentication\Models\UserToken;

interface AuthenticationObserverContract
{
    public function getGuardName(): string;

    public function getTokenLifetime($user): ?int;

    public function getNumberLimitedDevicesOnline($user): ?int;

    public function onBeforeLogin(UserToken $token): ?bool;

    public function onAfterLogin(UserToken $token, array $data = []);

    public function onLogout($user, ?UserToken $token);
}