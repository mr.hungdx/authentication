<?php


namespace HungDX\Authentication\Guards;

use Carbon\Carbon;
use HungDX\Authentication\AuthenticationService;
use HungDX\Authentication\Models\UserTokens;

class ApiGuard implements \Illuminate\Contracts\Auth\Guard
{
    use \Illuminate\Auth\GuardHelpers;

    private $config = [];

    private ?UserToken $user_token = null;

    protected \Illuminate\Http\Request $request;

    protected AuthenticationService $auth_service;

    protected bool $checked_token = false;

    public function __construct(
        \Illuminate\Contracts\Auth\UserProvide $provider,
        \Illuminate\Http\Request $request,
        $config
    )
    {
        $this->request = $request;
        $this->provider = $provider;
        $this->config = $config;
        $this->auth_service = app(AuthenticationService::class);
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Exception
     */
    public function user()
    {
        if (!$this->user && !$this->checked_token) {
            $this->checked_token = true;
            $this->user = $this->getUserFromToken();
        }

        return $this->user;
    }

    /**
     * Get user from user token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected function getUserFromToken()
    {
        $token = $this->request->header('Authorization', '');
        if (!$token) {
            return null;
        }

        $this->user_token = $this->auth_service->doLoginViaToken($token, false);
        if (!$this->user_token) {
            return null;
        }

        return $this->user_token->user;
    }

    public function login(\Illuminate\Contracts\Auth\Authenticatable $user)
    {
        $this->setUser($user);
        $this->user_token = new UserTokens();
        $this->user_token->user_id = $user->user_id;
        $this->user_token->refreshToken($this->auth_service->getTokenLifeTime($user));
    }

    /**
     * Validate a user's credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);
        return $this->hasValidCredentials($user, $credentials);
    }

    /**
     * Determine if the user matches the credentials.
     *
     * @param mixed $user
     * @param array $credentials
     * @return bool
     */
    protected function hasValidCredentials($user, $credentials)
    {
        return !is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }

    /**
     * Set the current request instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }
}
