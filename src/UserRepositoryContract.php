<?php


namespace HungDX\Authentication;


use Illuminate\Database\Eloquent\Model;

interface UserRepositoryContract
{
    /**
     * @param string $login_name
     * @return \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User|\App\User|Model|null
     */
    public function getUserByLoginName(string $login_name);

    /**
     * @param int $user_id
     * @return \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User|\App\User|Model|null
     */
    public function getUserByUserId(int $user_id);
}