<?php


namespace HungDX\Authentication;


use HungDX\Authentication\Models\UserToken;

trait AuthenticationObserverHelper
{
    public function getGuardName(): string
    {
        return config('auth.defaults.guard');
    }

    public function getTokenLifetime($user): ?int
    {
        return null;
    }

    public function getNumberLimitedDevicesOnline($user): ?int
    {
        return null;
    }

    public function onBeforeLogin(UserToken $token): ?bool
    {
        return null;
    }

    public function onAfterLogin(UserToken $token, array $data = [])
    {

    }

    public function onLogout($user, ?UserToken $token)
    {

    }
}