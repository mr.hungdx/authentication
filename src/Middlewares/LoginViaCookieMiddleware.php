<?php


namespace HungDX\Authentication\Middlewares;

use HungDX\Authentication\AuthenticationService;

class LoginViaCookieMiddleware
{
    /** @var AuthenticationService */
    private $service;

    /**
     * LoginViaCookieMiddleware constructor.
     * @param AuthenticationService $service
     */
    public function __construct(AuthenticationService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $token = $this->service->getCurrentToken();
        if (!$token) {
            $this->service->doLoginViaCookie();
            return $next($request);
        }

        if ($token->isExpired()) {
            $this->service->doLogout();
            $request->session()->flash('flash_danger', 'Your session have been expired');
            return $next($request);
        }

        $this->service->extendTokenLifeTime($token);
        return $next($request);
    }
}