<?php

namespace HungDX\Authentication\Middlewares;

use HungDX\Authentication\AuthenticationService;

class ApiGuardMiddleware
{
    /** @var AuthenticationService */
    private $service;

    /**
     * ApiGuardMiddleware constructor.
     * @param AuthenticationService $service
     */
    public function __construct(AuthenticationService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $response;
    }
}