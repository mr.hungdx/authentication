<?php

namespace HungDX\Authentication\Models;

use Carbon\Carbon;
use HungDX\Authentication\AuthenticationService;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserToken
 * @package App\Models
 * @property int $user_id
 * @property string $token
 * @property string $session_id
 * @property string|Carbon $created_at
 * @property string|Carbon $expired_at
 * @property string|Carbon $updated_at
 * @property \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User|\App\User|Model|null $user
 */
class UserToken extends Model
{
    private static $user_model_class_name = '';
    private static $user_model_primary_key = '';

    /** @var string */
    protected $table = 'user_tokens';

    /** @var string */
    protected $primaryKey = ['user_id', 'token'];

    /** @var bool */
    public $incrementing = false;

    /**
     * @return HasOne
     */
    public function user()
    {
        if (!self::$user_model_class_name) {
            $user_model_class_name = AuthenticationService::getConfig('user.model');
            if (!class_exists($user_model_class_name)) {
                throw new \Exception('User Model not found');
            }
            self::$user_model_class_name = $user_model_class_name;
            self::$user_model_primary_key = app($user_model_class_name)->getKeyName();
        }

        return $this->hasOne(self::$user_model_class_name, self::$user_model_primary_key, 'user_id');
    }

    public function isExpired(): bool
    {
        return (new Carbon())->gte($this->expired_at);
    }


    /**
     * @param string $token
     * @param $user_id
     * @return UserToken|null|Model
     */
    public static function getFromToken(string $token, $user_id): ?UserToken
    {
        if (empty($token) && empty($user_id)) {
            return null;
        }

        return self::query()
            ->where('user_id', $user_id)
            ->where('token', $token)
            ->first();
    }

    /**
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|UserToken[]
     */
    public static function getByUserId($user_id)
    {
        return self::query()
            ->where('user_id', $user_id)
            ->get();
    }

    public function refreshToken(int $timelife)
    {
        $this->session_id = session()->getId();
        $this->token = uniqid('login_') . '_' . time();
        $this->expired_at = Carbon::now()->addSeconds($timelife);
        $this->save();
    }

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     * @link https://stackoverflow.com/a/37076437
     */
    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     * @link https://stackoverflow.com/a/37076437
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}
